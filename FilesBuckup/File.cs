﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FilesBuckup
{
    public class File
    {
        private string path;


        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                this.path = value;
            }
        }


        public static List<File> FileInfoToFile(List<FileInfo> lst)
        {
            List<File> resp = new List<File>();
            foreach (FileInfo fi in lst)
            {
                File f = new File();
                f.Path = fi.FullName;
                resp.Add(f);
            }

            return resp;
        }
    }
}
