﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;

namespace FilesBuckup
{
    public partial class FilesBuckup : Form
    {
        Config config;

        public FilesBuckup()
        {
            InitializeComponent();
        }

        private void FilesBuckup_Load(object sender, EventArgs e)
        {
            config = new Config();

            config.loadXml("Config.xml");

            dgExt.DataSource = config.Extensiones;

            dgExt.Columns["extension"].ReadOnly = true;

            
            //Directory[] childDirs = dir.GetDirectories();

        }

        public List<File> GetFilesByExtensions(DirectoryInfo dir, string[] extensions)
        {
            if (extensions == null)
                throw new ArgumentNullException("extensions");

            var result = from f in dir.EnumerateFiles("*.*",SearchOption.AllDirectories) where (extensions.Contains(f.Extension.ToLower())) == true select f;

            return File.FileInfoToFile(result.ToList());

            //return result.ToList();
            //return dir.GetFiles("*.jpg",SearchOption.AllDirectories).ToList();
        }

        private bool isExtencion(string p, string[] extensions)
        {
            return extensions.Contains(p.ToLower());
        }



        private void btnCarpeta_Click(object sender, EventArgs e)
        {
            fbd.Description = "Seleccione la carpeta para buscar los archivos para realizar el buckup";

            DialogResult dr = fbd.ShowDialog();

            //MessageBox.Show(fbd.SelectedPath);

            if (fbd.SelectedPath != null && !fbd.SelectedPath.Equals(string.Empty))
            {
                txtFilesComp.Text = fbd.SelectedPath;

                List<File> dir = GetFilesByExtensions(new DirectoryInfo(txtFilesComp.Text), config.GetExtensionesHabilitadas());

                //List<string> paths = (from n in dir select n.FullName).ToList();

                

                dgArchivos.DataSource = dir;
            }

            
        }

        private void btnComprimir_Click(object sender, EventArgs e)
        {

            fbdGuardar.ShowDialog();

            string pathDestino = fbdGuardar.SelectedPath;

            List<File> lst = (List<File>)dgArchivos.DataSource;

            using (ZipFile zip = new ZipFile())
            {
                foreach (File f in lst)
                {
                    zip.AddFile(f.Path, Path.GetDirectoryName(f.Path).Replace(txtFilesComp.Text,string.Empty));
                }

                zip.Save(pathDestino + @"\"+txtNomProy.Text+"_"+DateTime.Now.ToString("yyyyMMdd_hhmmss")+".zip");
            }
        }

    }
}
