﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace FilesBuckup
{
    public class Propiedades
    {
        private string extension;
        private bool enabled;

        public string Extension
        {
            get
            {
                return extension;
            }
            set
            {
                extension = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

    }

    public class Config
    {
        private List<Propiedades> _extensiones;


        public List<Propiedades> Extensiones
        {
            get
            {
                return _extensiones;
            }
        }

        public Config()
        {
            _extensiones = new List<Propiedades>();
        }

        public void loadXml(string pathXml)
        {
            XmlDocument xDoc = new XmlDocument();

            //La ruta del documento XML permite rutas relativas
            //respecto del ejecutable!

            xDoc.Load(pathXml);

            XmlNodeList configuracion = xDoc.GetElementsByTagName("configuration");

            XmlNodeList extensiones = ((XmlElement)configuracion[0]).GetElementsByTagName("extensiones");

            XmlNodeList files = ((XmlElement)configuracion[0]).GetElementsByTagName("file");

            foreach (XmlElement file in files)
            {

                //XmlNodeList nExtension = file.GetElementsByTagName("extension");

                Propiedades p = xmlToExtension(file);
                _extensiones.Add(p);

            }

        }

        private Propiedades xmlToExtension(XmlNode xmlNode)
        {
            Propiedades p = new Propiedades();

            p.Enabled = xmlNode.Attributes["enabled"].Value.Equals("true") ? true : false;
            p.Extension = xmlNode.Attributes["extension"].Value;

            return p;
        }

        public string[] GetExtensionesHabilitadas()
        {
            return (from n in this.Extensiones where n.Enabled == true select n.Extension).ToArray();
        }
    }
}

