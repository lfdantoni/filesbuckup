﻿namespace FilesBuckup
{
    partial class FilesBuckup
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgExt = new System.Windows.Forms.DataGridView();
            this.txtFilesComp = new System.Windows.Forms.TextBox();
            this.btnCarpeta = new System.Windows.Forms.Button();
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.dgArchivos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnComprimir = new System.Windows.Forms.Button();
            this.fbdGuardar = new System.Windows.Forms.FolderBrowserDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNomProy = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgExt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArchivos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgExt
            // 
            this.dgExt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgExt.Location = new System.Drawing.Point(12, 90);
            this.dgExt.Name = "dgExt";
            this.dgExt.Size = new System.Drawing.Size(232, 171);
            this.dgExt.TabIndex = 0;
            // 
            // txtFilesComp
            // 
            this.txtFilesComp.Enabled = false;
            this.txtFilesComp.Location = new System.Drawing.Point(146, 12);
            this.txtFilesComp.Name = "txtFilesComp";
            this.txtFilesComp.Size = new System.Drawing.Size(261, 20);
            this.txtFilesComp.TabIndex = 1;
            // 
            // btnCarpeta
            // 
            this.btnCarpeta.Location = new System.Drawing.Point(413, 10);
            this.btnCarpeta.Name = "btnCarpeta";
            this.btnCarpeta.Size = new System.Drawing.Size(75, 23);
            this.btnCarpeta.TabIndex = 2;
            this.btnCarpeta.Text = "Examinar...";
            this.btnCarpeta.UseVisualStyleBackColor = true;
            this.btnCarpeta.Click += new System.EventHandler(this.btnCarpeta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Carpeta de busqueda";
            // 
            // dgArchivos
            // 
            this.dgArchivos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgArchivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArchivos.Location = new System.Drawing.Point(301, 90);
            this.dgArchivos.Name = "dgArchivos";
            this.dgArchivos.Size = new System.Drawing.Size(539, 171);
            this.dgArchivos.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Extensiones";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(298, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Archivos";
            // 
            // btnComprimir
            // 
            this.btnComprimir.Location = new System.Drawing.Point(765, 267);
            this.btnComprimir.Name = "btnComprimir";
            this.btnComprimir.Size = new System.Drawing.Size(75, 23);
            this.btnComprimir.TabIndex = 8;
            this.btnComprimir.Text = "Comprimir...";
            this.btnComprimir.UseVisualStyleBackColor = true;
            this.btnComprimir.Click += new System.EventHandler(this.btnComprimir_Click);
            // 
            // fbdGuardar
            // 
            this.fbdGuardar.Description = "Seleccione la ruta destino.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(250, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "<==>";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nombre Proyecto";
            // 
            // txtNomProy
            // 
            this.txtNomProy.Location = new System.Drawing.Point(146, 38);
            this.txtNomProy.Name = "txtNomProy";
            this.txtNomProy.Size = new System.Drawing.Size(261, 20);
            this.txtNomProy.TabIndex = 10;
            // 
            // FilesBuckup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 308);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNomProy);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnComprimir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgArchivos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCarpeta);
            this.Controls.Add(this.txtFilesComp);
            this.Controls.Add(this.dgExt);
            this.Name = "FilesBuckup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FilesBuckup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgExt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArchivos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgExt;
        private System.Windows.Forms.TextBox txtFilesComp;
        private System.Windows.Forms.Button btnCarpeta;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgArchivos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnComprimir;
        private System.Windows.Forms.FolderBrowserDialog fbdGuardar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNomProy;
    }
}

